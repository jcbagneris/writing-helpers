# -------------------------------------
# Makefile helpers
#
# JcB - 2018-01-18
#
# -------------------------------------

# Added to the .PHONY targets list of caller
HELPERS = help

print-%:
	@echo '$*=$($*) - $(flavor $*)'

help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

