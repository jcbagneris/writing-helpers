# -------------------------------------
# Makefile to produce pdf documents
# from latex sources and metadata
# in yaml format
#
# JcB - 2018-03-23
#
# -------------------------------------

# -------------------------------------
# source files
# -------------------------------------
FROM = $(firstword $(suffix $(wildcard $(BASENAME)-*.*)))
SOURCES = $(wildcard $(BASENAME)-*$(FROM))
GPGSOURCES = $(wildcard $(BASENAME)-*$(FROM).gpg)
PRIVSOURCES = $(wildcard $(BASENAME)-_*$(FROM))
PRIVGPGSOURCES = $(wildcard $(BASENAME)-_*$(FROM).gpg)
PUBSOURCES = $(filter-out $(PRIVSOURCES), $(SOURCES))
PUBGPGSOURCES = $(filter-out $(PRIVGPGSOURCES), $(GPGSOURCES))
INCLUDES = $(filter-out $(SOURCES), $(wildcard *$(FROM)))

# -------------------------------------
# paths
# -------------------------------------
PANDOCREP = $(wildcard ~/.local/share/pandoc)
BIN = $(wildcard ~/bin)
VPATH = $(PANDOCREP)/templates:$(PANDOCREP)/filters:$(PANDOCREP)/includes:$(BUILD):$(DEST)

# -------------------------------------
# metadata
# -------------------------------------
METADATA = $(wildcard *.yaml)

BASENAME = $(patsubst %.yaml,%,$(METADATA))
TITLE != grep ^title: $(METADATA) | cut -d " " -f 2- | tr -s " '" "__" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"
AUTHOR != grep ^author: $(METADATA) | $(BIN)/authors.py
DATE != grep "^date: [-/:.0-9]*" $(METADATA) | cut -d " " -f 2
VERSION != grep ^version: $(METADATA) | cut -d " " -f 2
TOPIC != grep ^topic: $(METADATA) | cut -d " " -f 2
SRCLANG != grep ^lang: $(METADATA) | cut -d " " -f 2
LEGACY != grep ^legacy_latest: $(METADATA) | cut -d " " -f 2

# -------------------------------------
# deps
# -------------------------------------
EXTDEPS = %-header.tex %-before-$(SRCLANG).tex %-after-$(SRCLANG).tex

# -------------------------------------
# vars
# -------------------------------------
# The BUILD directory SHOULD be a subdir of the local src one
BUILD = _build
DEST = _dest
TEXENGINE = xelatex
PREFIX = $(wildcard ~/work/finance)
INSTALLDIR = $(PREFIX)/_all
TIMESTAMP != date --utc +%Y%m%d%H%M%S
BUCKET := s3://files.bagneris.net
MAINBUILD := $(PREFIX)/src/_build

# -------------------------------------
# output vars
# -------------------------------------
FLAVOR = std
PATH_TO_LATEST = $(FLAVOR)/$(TOPIC)/$(SRCLANG)/
OUTPRE = $(TITLE)--$(AUTHOR)
OUTPOST = $(FLAVOR)--$(DATE).$(VERSION)

# -------------------------------------
# commands and options
# -------------------------------------
LATEXH = -H $(PANDOCREP)/includes/$*-header.tex
LATEXB = -B $(PANDOCREP)/includes/$*-before-$(SRCLANG).tex
LATEXA = -A $(PANDOCREP)/includes/$*-after-$(SRCLANG).tex
HIGHLIGHT = $(PANDOCREP)/includes/solarized-light.theme
PANDOCTO = latex
ifeq ($(FROM),.tex)
PANDOCOPTS = --standalone -t latex --metadata=date:v$(DATE).$(VERSION)
PANDOCARGS = \
$(LATEXH) $(LATEXB) -A $< $(LATEXA) \
$(BUILD)/$(BASENAME).yaml $(BUILD)/meta-$*.yaml -o $@
else
PANDOCOPTS = --standalone --highlight-style=$(HIGHLIGHT) -f markdown+raw_html+raw_tex -t $(PANDOCTO) --metadata=date:v$(DATE).$(VERSION) --filter=panflute --filter=pandoc-citeproc
PANDOCARGS = \
$(LATEXH) $(LATEXB) $(LATEXA) \
$(BUILD)/$(BASENAME).yaml $(BUILD)/meta-$*.yaml $(INCLUDES) $< -o $@
endif
RUBBER = rubber -m $(TEXENGINE) --into $(DEST)

# -------------------------------------
# targets
# -------------------------------------
TARGETS = $(patsubst $(BASENAME)-%$(FROM),%,$(SOURCES))
TARGETS += $(patsubst $(BASENAME)-%$(FROM).gpg,%,$(GPGSOURCES))
LATEXSOURCES = $(patsubst $(BASENAME)-%$(FROM),%-source,$(SOURCES))
LATEXSOURCES += $(patsubst $(BASENAME)-%$(FROM).gpg,%-source,$(GPGSOURCES))
UPLOADS = $(patsubst $(BASENAME)-%$(FROM),%-upload,$(PUBSOURCES))
UPLOADS += $(patsubst $(BASENAME)-%$(FROM).gpg,%-upload,$(PUBGPGSOURCES))
INSTALLS = $(patsubst $(BASENAME)-%$(FROM),%-install,$(PUBSOURCES))
INSTALLS += $(patsubst $(BASENAME)-%$(FROM).gpg,%-install,$(PUBGPGSOURCES))

# -------------------------------------
# rules
# -------------------------------------
.SUFFIXES: # Do not use standard suffix rules

#.SECONDARY: # Do not delete intermediary targets

.PHONY: help default all install upload rollback clean cleandeps cleanall $(TARGETS) $(LATEXSOURCES) $(UPLOADS) $(INSTALLS)

default: help ## Make help is the default, explicit is better than implicit

all: $(TARGETS) ## Make a pdf of the latest version and clean intermediary latex files from $(DEST) directory

install: $(INSTALLS) ## Hardlink pdfs and latex*html to local repo
ifeq ($(MAKELEVEL),0)
	-FLAVOR=$(FLAVOR) $(BIN)/dir-index > $(BUILD)/$(FLAVOR)-index.html
	-ln -f $(BUILD)/$(FLAVOR)-index.html $(INSTALLDIR)/$(FLAVOR)/index.html
ifeq ($(FLAVOR),std)
	-ln -f $(BUILD)/$(FLAVOR)-index.html $(INSTALLDIR)/index.html
endif
else
	@echo 'Not building index.html as called from main.mk'
endif

rollback:
	aws s3 cp $(MAINBUILD)/rollback.html $(BUCKET)/index.html --cache-control 'public, max-age=60'

upload: $(UPLOADS) ## Upload pdfs to S3 and updates the index
	$(BIN)/s3-index > $(BUILD)/index-$(TIMESTAMP).html
	aws s3 cp $(BUCKET)/index.html $(MAINBUILD)/rollback.html
	aws s3 cp $(BUILD)/index-$(TIMESTAMP).html $(BUCKET)/index-$(TIMESTAMP).html --cache-control 'public, max-age=31536000'
	aws s3 cp $(BUCKET)/index-$(TIMESTAMP).html $(BUCKET)/index.html --cache-control 'public, max-age=60'

clean: ## Delete $(BUILD) directory
	-rm -rf $(BUILD)

cleandeps: ## Delete $(BUILD)/*.d files
	- rm -f $(BUILD)/*.d

cleanall: clean ## Delete $(BUILD) and $(DEST) directories
	-rm -rf $(DEST)

slides handout _slides _handout: PANDOCTO := beamer

slides-install handout-install _slides-install _handout-install: PANDOCTO := beamer

slides-upload handout-upload _slides-upload _handout-upload: PANDOCTO := beamer

# We SHOULD cleandeps here, otherwise make {all,install,upload} when the $(TARGETS)
# exist already will rebuild it, but not clean it.
$(TARGETS): %: $(DEST)/$(OUTPRE)--%--$(OUTPOST).pdf cleandeps

$(LATEXSOURCES): %-source: $(BUILD)/$(BASENAME)-%-$(FLAVOR)-final.tex $(BUILD)/$(BASENAME)-%-$(FLAVOR)$(FROM) $(BUILD)/$(BASENAME).yaml $(BUILD)/meta-%.yaml

$(INSTALLS): %-install: $(DEST)/$(OUTPRE)--%--$(OUTPOST).pdf cleandeps | $(INSTALLDIR)/$(PATH_TO_LATEST)
	-ln -f -t $(INSTALLDIR)/$(PATH_TO_LATEST) $(DEST)/$(OUTPRE)--$*--$(OUTPOST).pdf

$(UPLOADS): %-upload: $(DEST)/$(OUTPRE)--%--$(OUTPOST).pdf cleandeps | $(INSTALLDIR)/$(PATH_TO_LATEST)
	aws s3 cp $(DEST)/$(OUTPRE)--$*--$(OUTPOST).pdf \
	$(BUCKET)/finance/$(PATH_TO_LATEST)$(OUTPRE)--$*--$(OUTPOST).pdf --cache-control 'public, max-age=31536000'

$(DEST)/$(OUTPRE)--%--$(OUTPOST).pdf: $(BUILD)/$(BASENAME)-%-$(FLAVOR)-final.tex | $(DEST)
	$(RUBBER) --jobname $(OUTPRE)--$*--$(OUTPOST) $<
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

$(BUILD)/$(BASENAME)-%-$(FLAVOR)-final.tex: $(BUILD)/$(BASENAME)-%-$(FLAVOR)$(FROM) $(BUILD)/$(BASENAME).yaml $(BUILD)/meta-%.yaml $(EXTDEPS)
	pandoc $(PANDOCOPTS) $(PANDOCARGS)
	sed -i 's/excludecomment{$(FLAVOR)}/includecomment{$(FLAVOR)}/' $@
	sed -i 's/VERSION/$(VERSION)/' $@
	sed -i 's/SRCLANG/$(SRCLANG)/' $@
	sed -i 's;FLAVOR;$(FLAVOR);' $@
	sed -i 's;TYPEDOC;$*;' $@
	sed -i 's;PDF;$(OUTPRE)--$*--$(OUTPOST).pdf;g' $@

$(BUILD)/meta-%.yaml: | $(BUILD)
	touch $(BUILD)/meta-$*.yaml

$(BUILD)/$(BASENAME)-%-$(FLAVOR).tex: $(BASENAME)-%.tex | $(BUILD)
	awk 'BEGIN{x="$@";}/^---$$/{++i;}i==1{x="$(BUILD)/meta-$*.yaml";}i==2{print "---">x;i++;x="$@";next}{print>x;}' $<

$(BUILD)/$(BASENAME)-%-$(FLAVOR).pmd: $(BASENAME)-%.pmd | $(BUILD)
	-cp -u $< $@

$(BUILD)/%.tex: %.tex | $(BUILD)
	-cp $*.tex $(BUILD)/

$(BUILD)/$(BASENAME).yaml: $(BASENAME).yaml
	-cp -u $< $@

$(DEST) $(BUILD) $(INSTALLDIR)/$(PATH_TO_LATEST):
	mkdir -p $@

$(BUILD)/%-$(FLAVOR).d: %.tex | $(BUILD)
	FLAVOR=$(FLAVOR) $(BIN)/latex-depends.py $< > $@

%.tex: %.tex.gpg
	gpg2 --output $@ --decrypt $<

%.pmd: %.pmd.gpg
	gpg2 --output $@ --decrypt $<

ifneq ($(FROM),.pmd)
ifneq ($(MAKECMDGOALS),cleanall)
include $(patsubst %$(FROM),$(BUILD)/%-$(FLAVOR).d,$(SOURCES))
endif
endif

include helpers.mk
