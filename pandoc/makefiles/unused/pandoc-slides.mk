# Makefile pour produire des slides/handouts latex
# à partir de source pandoc markdown
#
# JcB - 2016-09-01
#

PANDOCREP = $(wildcard ~/.pandoc)
VPATH = helpers:..
FLAVOR =
LANG = en
SOURCE = $(wildcard *.pmd)
ifeq ($(FLAVOR),mbs)
	SLIDESDEPS = slides-$(LANG)-mbs.tex preamble-beamer-en-blue.tex logos/logo.png logos/logos.png
	HANDOUTDEPS = handout-$(LANG)-mbs.tex preamble-beamer-en-blue.tex title-handout-mbs.tex
else
	SLIDESDEPS = slides-$(LANG)-std.tex preamble-beamer-en-blue.tex
	HANDOUTDEPS = handout-$(LANG)-std.tex preamble-beamer-en-blue.tex title-handout.tex
endif
RUBBERDEPS = preamble-beamer-en-common.tex preamble-standard-packages.tex preamble-newcommands.tex
TEXSOURCE = ._source.tex
AUTHOR != grep ^author: $(SOURCE) | $(BIN)/authors.py
VERSION != grep ^version $(SOURCE) | cut -d " " -f 2
DATE != grep ^date $(SOURCE) | cut -d " " -f 2
TITLE = $(basename $(SOURCE))
ifeq ($(FLAVOR),)
	NAME = $(TITLE)_$(VERSION)--$(AUTHOR)--$(DATE)
else
	NAME = $(TITLE)_$(VERSION)--$(AUTHOR)--$(FLAVOR)--$(DATE)
endif
PDFSLIDES = $(NAME)--slides.pdf
PDFHANDOUT = $(NAME)--handout.pdf

RUBBER = rubber --pdf --into ..
PANDOC = pandoc -s -t beamer --template jcb

default: help ## Make help is the default, explicit is better than implicit

all: slides handout ## Make the slides and handout pdf targets, default

.PHONY: all clean cleanall $(HELPERS)

clean: ## Delete latex compilation files from src and ../src
	-rm -f *log *aux *toc *snm *nav *out *bbl *blg .*tex
	-rm -f ../*log ../*aux ../*toc ../*snm ../*nav ../*out ../*bbl ../*blg

cleanall: clean ## Delete pdf target as well as latex compilation files from src and ../src
	-rm -f ../*pdf

$(TEXSOURCE): $(SOURCE)
	$(PANDOC) $^ -o $@
	sed -i 's/date{\(.*\)\.1}/date{\1.10}/' $@

._slides_$(FLAVOR).tex: $(SLIDESDEPS)
	cp $< $@

._handout_$(FLAVOR).tex: $(HANDOUTDEPS)
	cp $< $@

$(PDFSLIDES): ._slides_$(FLAVOR).tex $(TEXSOURCE)

$(PDFHANDOUT): ._handout_$(FLAVOR).tex $(TEXSOURCE)

slides: $(PDFSLIDES) ## Make the slides pdf target

handout: $(PDFHANDOUT) ## Make the handout pdf targett

# Implicit rules

$(NAME)--%.pdf: ._%_$(FLAVOR).tex $(RUBBERDEPS)
	$(RUBBER) --jobname $(NAME)--$* $<

include helpers.mk
