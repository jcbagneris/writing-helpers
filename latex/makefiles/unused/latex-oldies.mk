# -------------------------------------
# Makefile to produce pdf handouts
# from various sources
#
# JcB - 2017-08-18
#
# -------------------------------------

# -------------------------------------
# source file, should be unique
# -------------------------------------
SOURCES = $(wildcard *.tex *.pmd *.rst)
SOURCE = $(filter-out $(wildcard version.* *-src.*), $(SOURCES))

# -------------------------------------
# paths
# -------------------------------------
PANDOCREP = $(wildcard ~/.pandoc)
LATEXREP =
BIN = $(wildcard ~/bin)
VPATH = $(PANDOCREP)/templates:$(PANDOCREP)/filters:$(PANDOCREP)/includes:$(DEST)

# -------------------------------------
# vars
# -------------------------------------
DEST = ..
FLAVOR = master

# -------------------------------------
# metadata
# -------------------------------------
AUTHOR != awk 'match($$0, /^\\author{.* (.*)}$$/, arr) {print arr[1]}' $(SOURCE)
VERSION != awk 'match($$0, /{\\versiondate}{v(.*)}$$/, arr) {print arr[1]}' version.tex
TITLE != awk 'match($$0, /^\\title{(.*)}$$/, arr) {print arr[1]}' $(SOURCE) | tr -s " " "_" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"

# -------------------------------------
# manage FLAVOR
# -------------------------------------
ifeq ($(FLAVOR),master)
	NAME = $(TITLE)--$(AUTHOR)--$(VERSION)
else
	NAME = $(TITLE)--$(AUTHOR)--$(FLAVOR)--$(VERSION)
endif

# -------------------------------------
# intermediary targets
# -------------------------------------

# -------------------------------------
# dependencies
# -------------------------------------
PANDOCDEPS =
LATEXDEPS =

# -------------------------------------
# commands and options
# -------------------------------------
RUBBER = rubber --pdf --into $(DEST)

# -------------------------------------
# output
# -------------------------------------
PDF = $(NAME).pdf

# -------------------------------------
# targets
# -------------------------------------
.PHONY: default all clean cleandest cleanall spellcheck latex pdf upload $(HELPERS)

default: help ## Make help is the default, explicit is better than implicit

all: pdf cleandest ## Make a pdf of the latest version and clean intermediary latex files from $(DEST) directory

cleandest: ## Delete intermediary latex files from $(DEST) directory
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

cleanall: cleandest ## Delete pdf target as well as latex compilation files from $(DEST)
	-rm -f $(DEST)/*pdf

spellcheck: $(SOURCE) ## Spell checking with aspell
	aspell --mode tex --lang $(SRCLANG) --check $<

$(DEST):
	mkdir -p $@

pdf: $(PDF) ## Make the target pdf file

$(PDF): $(SOURCE) | $(DEST)
#	We use rubber to build the pdf as its heuristics to guess how many passes
#	are necessary are better than the ones of pandoc it seems.
	-git co $(FLAVOR)
	$(RUBBER) --jobname $(NAME) $<

include helpers.mk
