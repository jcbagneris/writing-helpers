"""
Pandoc filter to render div blocks as environments e.g. in latex
The environment should be defined somewhere (hint: in the latex template)

$ cat mwe-example-blocks.md
---
panflute-filters: [blocks, ]
...

::: {.example #ex1 title="A nice example"}
This is a numbered example.
:::

:::{- .example #ex2 title="Another example"}
This is another example, unnumbered
:::


$ pandoc -f markdown -t latex --filter=panflute mwe-example-blocks.md
\hypertarget{ex1}{}
\begin{example}[A nice example]
\label{ex1}

This is a numbered example.

\end{example}

\hypertarget{ex2}{}
\begin{example*}[Another example]
\label{ex2}

This is another example, unnumbered

\end{example*}

"""

import panflute as pf


BLOCKS = {'example','definition','note','block'}
EXAMPLE = {'en': 'Example',
           'fr': 'Exemple',
           'vn': u'ví dụ',
           }


def prepare(doc):
    pass


def finalize(doc):
    pass

def writelatex(block, div, doc):
    if 'unnumbered' in div.classes:
        block = '%s*' % block
    if div.identifier:
        openraw = pf.RawBlock('\\begin{%s}[%s]\n\label{%s}'
                                % (block,
                                   div.attributes.get('title', ''),
                                   div.identifier),
                                format='latex')
    else:
        openraw = pf.RawBlock('\\begin{%s}[%s]' % (block, div.attributes.get('title', '')), format='latex')
    closeraw = pf.RawBlock('\\end{%s}' % block, format='latex')
    div.content.insert(0, openraw)
    div.content.append(closeraw)
    return div


def writebeamer(block, div, doc):
    HASTITLE = {'block'}
    if block in HASTITLE:
        if div.identifier:
            openraw = pf.RawBlock('\\begin{%s}[label={%s}]{%s}'
                                    % (block,
                                       div.identifier,
                                       div.attributes.get('title', block)),
                                    format='latex')
        else:
            openraw = pf.RawBlock('\\begin{%s}{%s}' % (block, div.attributes.get('title', block)), format='latex')
    else:
        if div.identifier:
            openraw = pf.RawBlock('\\begin{%s}\label{%s}\n' % (block, div.identifier), format='latex')
        else:
            openraw = pf.RawBlock('\\begin{%s}' % block, format='latex')
    closeraw = pf.RawBlock('\\end{%s}' % block, format='latex')
    div.content.insert(0, openraw)
    div.content.append(closeraw)
    return div

def nochange(block, div, doc):
    return

def action(elem, doc):
    WRITERS = {'latex': writelatex, 'beamer': writebeamer}
    if isinstance(elem, pf.Div):
        try:
            block = BLOCKS.intersection(set(elem.classes)).pop()
        except KeyError:
            # intersection empty, pop() fails:
            # the div class is not in BLOCKS, let it pass unaltered
            return
        return WRITERS.get(doc.format, 'nochange')(block, elem, doc)
        


def main(doc=None):
    return pf.run_filter(action,
                        prepare=prepare,
                        finalize=finalize,
                        doc=doc)


if __name__ == '__main__':
    main()

