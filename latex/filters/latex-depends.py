#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
latex-depends.py

Recursively build a list of tex files dependencies.
Deps are defined by an \input{foobar} line.
Returns a GNU make prerequisites line such as:
$(BUILD)/toto.tex $(BUILD)/toto.d: $(BUILD)/foo.tex helpers/bar.tex

Honors the FLAVOR environnement variable.

Usage: FLAVOR=std latex-depends.py toto.tex > toto.d
"""

import sys
import re
import os
import subprocess

from pathlib import Path

FLAVOR = os.environ.get('FLAVOR','std')
BUILD = '$(BUILD)'

inputtarget = re.compile(r'[^%]*\\input{(.*?)}')
graphicstarget = re.compile(r'[^%]*\\includegraphics.*{(.*?)}')

def render(pathlist, sep=" ",prefix="", suffix="\n"):
    strtuple = (p.as_posix() for p in pathlist)
    rendered = "{}{}{}".format(prefix,sep.join(strtuple),suffix)
    return rendered


def parse(tf):
    deps = []
    with tf.open('rt') as f:
        for line in f:
            mtarget = inputtarget.match(line)
            gtarget = graphicstarget.match(line)
            if mtarget:
                ftarget = Path(mtarget.group(1).replace('FLAVOR',FLAVOR)).with_suffix('.tex')
                args = ['kpsewhich', str(ftarget)]
                sp = subprocess.run(args, stdout=subprocess.PIPE, check=True, universal_newlines=True)
                ftarget = Path(sp.stdout.strip("\n"))
                if (ftarget in deps) or (BUILD / ftarget in deps):
                    raise RecursionError("Circular error : {} already parsed.\n".format(ftarget))
                else:
                    if ftarget.parent == Path('helpers'):
                        deps.append(ftarget)
                        deps.extend(parse(ftarget))
                    else:
                        deps.append(BUILD / ftarget)
                        deps.extend(parse(ftarget))
            if gtarget:
                ftarget = Path(gtarget.group(1).replace('FLAVOR',FLAVOR))
                deps.append(ftarget)
    return deps



texfile = Path(sys.argv[1])
buildtexfile = Path('{}-{}.tex'.format(texfile.stem, FLAVOR))
dfile = buildtexfile.with_suffix('.d')
head = "{0}/{1} {0}/{2} : ".format(BUILD, buildtexfile, dfile)

sys.stdout.write(head)
sys.stdout.write(render(parse(texfile)))

