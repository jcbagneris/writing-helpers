# -------------------------------------
# Makefile to produce pdf case studies
# from latex sources
#
# JcB - 2018-01-11
#
# -------------------------------------

# -------------------------------------
# source files
# -------------------------------------
SOURCES = $(wildcard *-*.tex)
NAME = $(subst -text,,$(basename $(wildcard *-text.tex)))
META = meta.tex

# -------------------------------------
# paths
# -------------------------------------
BIN = $(wildcard ~/bin)
#VPATH = $(wildcard ~/texmf/tex/latex)

# -------------------------------------
# vars
# -------------------------------------
DEST = _dest
BUILD = _build

# -------------------------------------
# metadata
# -------------------------------------
AUTHOR != awk 'match($$0, /^\\author{.*[ ~](.*)}$$/, arr) {print arr[1]}' $(META)
VERSION != awk 'match($$0, /{\\versiondate}{v(.*)}$$/, arr) {print arr[1]}' $(META)
TITLE != awk 'match($$0, /^\\title{(.*)}$$/, arr) {print arr[1]}' $(META) | tr -s " " "_" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"

# -------------------------------------
# manage FLAVOR
# -------------------------------------
FLAVOR = std

# -------------------------------------
# targets
# -------------------------------------
TARGETS = $(patsubst $(NAME)-%.tex,%,$(SOURCES))

# -------------------------------------
# commands and options
# -------------------------------------
RUBBER = rubber --pdf --into $(DEST)

# -------------------------------------
# rules
# -------------------------------------
.SECONDARY: # Do not delete intermediary targets

.PHONY: default all cleandest cleanall $(TARGETS) $(HELPERS)

default: help ## Make help is the default, explicit is better than implicit

all: $(TARGETS) ## Make pdfs of the latest version and clean intermediary latex files from $(DEST) directory

cleandest: ## Delete intermediary latex files from $(DEST) directory
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

cleanall: ## Delete $(DEST) and $(BUILD)
	-rm -rf $(DEST)
	-rm -rf $(BUILD)

$(TARGETS): %: $(DEST)/$(TITLE)_%--$(AUTHOR)--$(FLAVOR)--$(VERSION).pdf

$(BUILD)/$(NAME)-%-$(FLAVOR).tex: $(NAME)-%.tex | $(BUILD)
	-cp $< $@
	sed -i 's;FLAVOR;$(FLAVOR);' $@

$(BUILD)/%.tex: %.tex | $(BUILD)
	-cp $*.tex $(BUILD)/

$(DEST)/$(TITLE)_%--$(AUTHOR)--$(FLAVOR)--$(VERSION).pdf: $(BUILD)/$(NAME)-%-$(FLAVOR).tex $(META) | $(DEST)
	$(RUBBER) --jobname $(TITLE)_$*--$(AUTHOR)--$(FLAVOR)--$(VERSION) $<

$(DEST) $(BUILD):
	mkdir -p $@

$(BUILD)/%-$(FLAVOR).d: %.tex | $(BUILD)
	FLAVOR=$(FLAVOR) $(BIN)/latex-depends.py $< > $@

ifneq ($(MAKECMDGOALS),cleanall)
include $(patsubst %.tex,$(BUILD)/%-$(FLAVOR).d,$(SOURCES))
endif

include helpers.mk
