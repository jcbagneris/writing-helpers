#!/usr/bin/env python3
"""
dir-index

Output the list of a directory contents
"""

import sys
import os
import os.path
import time
from datetime import datetime, timezone

from pathlib import Path, PurePath

REPO = 'work/finance/_all'
FLAVOR = os.environ.get('FLAVOR','std')
TRB = '<tr>\n'
TRA = '</tr>\n'
TD = '\t<td></td>\n'
LANG = '\t<td>{Lang}</td>\n'
THEME = '\t<td>{Theme}</td>\n'
LINK = '\t<td><a href=\'{Url}\'>{File}</a></td>\n'
LAST = '\t<td style="text-align: right;">{LastModified}</td>\n'
SIZE = '\t<td style="text-align: right;">{Size:,}</td>\n'
VERSION = '\t<td style="text-align: right;">{Version}</td>\n'
BEFORE = """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<title>Index</title>
<style type="text/css">
    body {font-family: Arial, Verdana, sans-serif;}
    tr:hover {background-color: #eee;}
</style>
</head>
<body>
<table style="width: 80%; padding: 10px; border-collapse: collapse; border-bottom: solid 1px black;" cellpadding="5" cellspacing="3">
<thead>
<tr style="border-bottom: 1px solid black; background-color: #eee;">
    <th style="text-align: left;">Section</th>
    <th style="text-align: left;">Lang</th>
    <th style="text-align: left;">File</th>
    <th style="text-align: right;">Size</th>
    <th style="text-align: right;">Version</th>
    <th style="text-align: right;">Last Modified</th>
</tr>
</thead>
<tfoot></tfoot>
<tbody>
"""
AFTER= """</tbody>
</table>
<p style="font-size: smaller;">
Sources:
<a href="https://gitlab.com/jcbagneris/finance-sources">
https://gitlab.com/jcbagneris/finance-sources
</a>
- Last update: {update}
</p>
</body>
</html>
"""

home = Path.home()
repo = Path(REPO)
p = home / repo / FLAVOR
pdfs = list(p.glob('**/*.pdf'))
allpdfs = []

for pdf in pdfs:
    _ = dict()
    pdfparts = pdf.parts
    _['Key'] = pdf
    _['Url'] = PurePath('').joinpath(*pdfparts[7:]).as_posix()
    _['Size'] = os.path.getsize(_['Key'])
    _['LastModified'] = time.strftime('%Y-%m-%d',time.localtime(os.path.getmtime(_['Key'])))
    _['File'] = pdfparts[-1]
    _['Lang'] = pdfparts[-2].capitalize()
    #_['Theme'] = pdfparts[7].capitalize()
    _['Theme'] = ' / '.join(map(str.capitalize,pdfparts[7:9]))
    _['Title'],_['Author'],_['Typedoc'],rawversion = _['File'].split('--', maxsplit=3)
    version = rawversion.rsplit('--')[-1]
    _['Version'] = version.rstrip('.pdf')
    year,month,number = _['Version'].split('.')
    _['Vernum'] = int(year)*100 + int(month)*10 + int(number)
    allpdfs.append(_)

allpdfs.sort(key=lambda x: (x['Theme'],x['Lang'],x['Title'],x['Typedoc'],-x['Vernum']))

sys.stdout.write(BEFORE)
theme = ''
lang = ''
title = ''
typedoc = ''
for pdf in allpdfs:
    sys.stdout.write(TRB)
    if pdf['Theme'] == theme:
        sys.stdout.write(TD)
    else:
        sys.stdout.write(THEME.format(**pdf))
        theme = pdf['Theme']
        lang = ''
        title = ''
        typedoc = ''
    if pdf['Lang'] == lang:
        sys.stdout.write(TD)
    else:
        sys.stdout.write(LANG.format(**pdf))
        lang = pdf['Lang']
        title = ''
        typedoc = ''
    title = pdf['Title']
    typedoc = pdf['Typedoc']
    sys.stdout.write(LINK.format(**pdf))
    sys.stdout.write(SIZE.format(**pdf))
    sys.stdout.write(VERSION.format(**pdf))
    sys.stdout.write(LAST.format(**pdf))
    sys.stdout.write(TRA)
sys.stdout.write(
        AFTER.format(
            update=datetime.now(timezone.utc).isoformat(
                sep=' ',timespec='seconds')))

