#!/usr/bin/env python3
"""
s3-index

Output the list of an S3 bucket's contents
"""

import sys
import boto3
from datetime import datetime, timezone

BUCKET = 'files.bagneris.net'
PREFIX= 'finance'
TRB = '<tr>\n'
TRA = '</tr>\n'
TD = '\t<td></td>\n'
LANG = '\t<td>{Lang}</td>\n'
THEME = '\t<td>{Theme}</td>\n'
LINK = '\t<td><a href=\'https://files.bagneris.net/{Key}\'>{File}</a></td>\n'
LAST = '\t<td style="text-align: right;">{LastModified:%Y-%m-%d}</td>\n'
SIZE = '\t<td style="text-align: right;">{Size:,}</td>\n'
VERSION = '\t<td style="text-align: right;">{Version}</td>\n'
BEFORE = """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<title>Index</title>
<style type="text/css">
    body {font-family: Arial, Verdana, sans-serif;}
    tr:hover {background-color: #eee;}
</style>
</head>
<body>
<table style="width: 80%; padding: 10px; border-collapse: collapse; border-bottom: solid 1px black;" cellpadding="5" cellspacing="3">
<thead>
<tr style="border-bottom: 1px solid black; background-color: #eee;">
    <th style="text-align: left;">Section</th>
    <th style="text-align: left;">Lang</th>
    <th style="text-align: left;">File</th>
    <th style="text-align: right;">Size</th>
    <th style="text-align: right;">Version</th>
    <th style="text-align: right;">Last Modified</th>
</tr>
</thead>
<tfoot></tfoot>
<tbody>
"""
AFTER= """</tbody>
</table>
<p style="font-size: smaller;">
Sources:
<a href="https://gitlab.com/jcbagneris/finance-sources">
https://gitlab.com/jcbagneris/finance-sources
</a>
- Last update: {update}
</p>
</body>
</html>
"""


s3 = boto3.client('s3')
response = s3.list_objects(Bucket=BUCKET, Prefix=PREFIX)
allpdfs = [
        _
        for _ in response['Contents'] 
        if (
            _['Key'].endswith('.pdf')
            and 
            _['Key'].startswith('finance/std')
            )
        ]

for pdf in allpdfs:
    allfields = pdf['Key'].rsplit('/')
    #path,flavor,pdf['Theme'] = allfields[0:3]
    path,flavor = allfields[0:2]
    pdf['Theme'] = ' / '.join(map(str.capitalize,allfields[2:4]))
    pdf['File'] = allfields[-1]
    pdf['Lang'] = allfields[-2]
    pdf['Title'],pdf['Author'],pdf['Typedoc'],rawversion = pdf['File'].split('--', maxsplit=3)
    version = rawversion.rsplit('--')[-1]
    pdf['Version'] = version.rstrip('.pdf')
    pdf['Lang'] = pdf['Lang'].capitalize()
    #pdf['Theme'] = pdf['Theme'].capitalize()
    year,month,number = pdf['Version'].split('.')
    pdf['Vernum'] = int(year)*100 + int(month)*10 + int(number)

allpdfs.sort(key=lambda x: (x['Theme'],x['Lang'],x['Title'],x['Typedoc'],-x['Vernum']))

sys.stdout.write(BEFORE)
theme = ''
lang = ''
title = ''
typedoc = ''
same = False
for pdf in allpdfs:
    if (pdf['Title'] == title) and (pdf['Typedoc'] == typedoc):
        continue
    if pdf['Typedoc'].startswith('_'):
        continue
    sys.stdout.write(TRB)
    if pdf['Theme'] == theme:
        sys.stdout.write(TD)
    else:
        sys.stdout.write(THEME.format(**pdf))
        theme = pdf['Theme']
        lang = ''
        title = ''
        typedoc = ''
    if pdf['Lang'] == lang:
        sys.stdout.write(TD)
    else:
        sys.stdout.write(LANG.format(**pdf))
        lang = pdf['Lang']
        title = ''
        typedoc = ''
    title = pdf['Title']
    typedoc = pdf['Typedoc']
    sys.stdout.write(LINK.format(**pdf))
    sys.stdout.write(SIZE.format(**pdf))
    sys.stdout.write(VERSION.format(**pdf))
    sys.stdout.write(LAST.format(**pdf))
    sys.stdout.write(TRA)
sys.stdout.write(
        AFTER.format(
            update=datetime.now(timezone.utc).isoformat(
                sep=' ',timespec='seconds')))

