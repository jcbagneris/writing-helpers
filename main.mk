# -------------------------------------
# Makefile to recursively make
# all documents
#
# JcB - 2018-06-11
#
#  inspiration:
#  https://stackoverflow.com/questions/11206500/compile-several-projects-with-makefile-but-stop-on-first-broken-build/11206700#11206700

# -------------------------------------
# subdirectories
# -------------------------------------
SUBDIRS !=  find ./ -name Makefile -printf '%h/. '

# -------------------------------------
# targets
# -------------------------------------
TARGETS := all install
SUBDIRS_TARGETS := $(foreach t,$(TARGETS),$(addsuffix $t,$(SUBDIRS)))

# -------------------------------------
# paths
# -------------------------------------
BIN = $(wildcard ~/bin)

# -------------------------------------
# vars
# -------------------------------------
# The BUILD directory SHOULD be a subdir of the local src one
BUILD = _build
FLAVOR = std
PREFIX = $(wildcard ~/work/finance)
INSTALLDIR = $(PREFIX)/_all
TIMESTAMP != date --utc +%Y%m%d%H%M%S
BUCKET := s3://files.bagneris.net

# -------------------------------------
# rules
# -------------------------------------

.PHONY: $(TARGETS) $(SUBDIR_TARGETS) upload rollback

upload-current:
	aws s3 sync $(INSTALLDIR)/$(FLAVOR)/ $(BUCKET)/finance/$(FLAVOR)/ --exclude "index.html" --cache-control 'public, max-age=3156000'
	$(BIN)/s3-index > $(BUILD)/index-$(TIMESTAMP).html
	aws s3 cp $(BUCKET)/index.html $(BUILD)/rollback.html
	aws s3 cp $(BUILD)/index-$(TIMESTAMP).html $(BUCKET)/index-$(TIMESTAMP).html --cache-control 'public, max-age=31536000'
	aws s3 cp $(BUCKET)/index-$(TIMESTAMP).html $(BUCKET)/index.html --cache-control 'public, max-age=60'

upload: install
	aws s3 sync $(INSTALLDIR)/$(FLAVOR)/ $(BUCKET)/finance/$(FLAVOR)/ --exclude "index.html" --cache-control 'public, max-age=3156000'
	$(BIN)/s3-index > $(BUILD)/index-$(TIMESTAMP).html
	aws s3 cp $(BUCKET)/index.html $(BUILD)/rollback.html
	aws s3 cp $(BUILD)/index-$(TIMESTAMP).html $(BUCKET)/index-$(TIMESTAMP).html --cache-control 'public, max-age=31536000'
	aws s3 cp $(BUCKET)/index-$(TIMESTAMP).html $(BUCKET)/index.html --cache-control 'public, max-age=60'

install: $(addsuffix install,$(SUBDIRS)) | $(BUILD)
	FLAVOR=$(FLAVOR) $(BIN)/dir-index > $(BUILD)/$(FLAVOR)-index.html
	-ln -f $(BUILD)/$(FLAVOR)-index.html $(INSTALLDIR)/$(FLAVOR)/index.html
ifeq ($(FLAVOR),std)
	-ln -f $(BUILD)/$(FLAVOR)-index.html $(INSTALLDIR)/index.html
endif


rollback:
	aws s3 cp $(BUILD)/rollback.html $(BUCKET)/index.html --cache-control 'public, max-age=60'


all: $(addsuffix all,$(SUBDIRS))

$(SUBDIRS_TARGETS):
	$(MAKE) -Rr -C $(@D) $(@F:.%=%)

$(BUILD) $(INSTALLDIR):
	mkdir -p $@

include helpers.mk
