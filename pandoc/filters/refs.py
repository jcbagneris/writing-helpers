"""
Pandoc filter to replace empty links in markdown with \ref{} in latex output

$ cat mwe-example-refs.md
---
panflute-filters: [refs, ]
...

# Section 1

Blah foo bar

# Section 2

Please see also [](#section-1)


$ pandoc -f markdown -t latex --filter=panflute mwe-example-refs.md
\hypertarget{section-1}{%
\section{Section 1}\label{section-1}}

Blah foo bar

\hypertarget{section-2}{%
\section{Section 2}\label{section-2}}

Please see also \protect\ref{section-1}

"""

import panflute as pf



def prepare(doc):
    pass


def finalize(doc):
    pass

def writelatex(link, doc):
    if link.url.startswith('#'):
        return pf.RawInline('\\protect\\ref{%s}' % link.url.lstrip('#'), format='latex')
    else:
        return


writebeamer = writelatex

def nochange(link, doc):
    return

def action(elem, doc):
    WRITERS = {'latex': writelatex, 'beamer': writebeamer}
    if isinstance(elem, pf.Link) and not elem.content:
        return WRITERS.get(doc.format, 'nochange')(elem, doc)
        


def main(doc=None):
    return pf.run_filter(action,
                        prepare=prepare,
                        finalize=finalize,
                        doc=doc)


if __name__ == '__main__':
    main()

