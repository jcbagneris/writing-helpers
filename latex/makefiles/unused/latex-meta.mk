# -------------------------------------
# Makefile to produce pdf handouts
# from latex sources including metadata
# in yaml format
#
# JcB - 2017-08-28
#
# -------------------------------------

# -------------------------------------
# source file, should be unique
# -------------------------------------
SOURCES = $(wildcard *.tex *.bib *.yaml)
SOURCE = $(filter-out $(wildcard version.* *-src.* *.bib *.yaml), $(SOURCES))

# -------------------------------------
# paths
# -------------------------------------
PANDOCREP = $(wildcard ~/.pandoc)
LATEXREP =
BIN = $(wildcard ~/bin)
VPATH = $(PANDOCREP)/templates:$(PANDOCREP)/filters:$(PANDOCREP)/includes:$(BUILD):$(DEST)

# -------------------------------------
# vars
# -------------------------------------
# The BUILD directory SHOULD be a subdir of the local src one
BUILD = _build
DEST = _dest
TEXENGINE = xelatex

# -------------------------------------
# metadata
# -------------------------------------
METADATA = $(wildcard *.yaml)

TITLE != grep ^title: $(METADATA) $(SOURCE) | cut -d " " -f 2- | tr -s " " "_" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"
AUTHOR != grep ^author: $(METADATA) $(SOURCE) | $(BIN)/authors.py
DATE != grep "^date: [-/:0-9]*" $(METADATA) $(SOURCE) | cut -d " " -f 2
VERSION != grep ^version: $(METADATA) $(SOURCE) | cut -d " " -f 2
TOPIC != grep ^topic: $(METADATA) $(SOURCE) | cut -d " " -f 2
TYPEDOC != grep ^typedoc: $(METADATA) $(SOURCE) | cut -d " " -f 2
SRCLANG != grep ^lang: $(METADATA) $(SOURCE) | cut -d " " -f 2

# -------------------------------------
# manage FLAVOR
# -------------------------------------
FLAVOR = std
ifeq ($(FLAVOR),std)
	NAME = $(TITLE)--$(AUTHOR)--$(DATE).$(VERSION)
else
	NAME = $(TITLE)--$(AUTHOR)--$(FLAVOR)--$(DATE).$(VERSION)
endif

# -------------------------------------
# intermediary targets
# -------------------------------------
FINALSOURCE = $(BUILD)/source-$(FLAVOR).tex

# -------------------------------------
# dependencies
# -------------------------------------
LATEXDEPS = $(TYPEDOC)-header-$(SRCLANG).tex $(TYPEDOC)-before-$(SRCLANG).tex $(TYPEDOC)-after-$(SRCLANG).tex

# -------------------------------------
# commands and options
# -------------------------------------
LATEXH = -H $(PANDOCREP)/includes/$(TYPEDOC)-header-$(SRCLANG).tex
LATEXB = -B $(PANDOCREP)/includes/$(TYPEDOC)-before-$(SRCLANG).tex
LATEXA = -A $(PANDOCREP)/includes/$(TYPEDOC)-after-$(SRCLANG).tex
ADDSOURCE =
PANDOCOPTS = --standalone -t latex
RUBBER = rubber -m $(TEXENGINE) --into $(DEST)

# -------------------------------------
# output
# -------------------------------------
PDF = $(NAME).pdf
PATH_TO_LATEST = $(FLAVOR)/$(TOPIC)/$(SRCLANG)/

# -------------------------------------
# targets
# -------------------------------------
.SUFFIXES:

.PHONY: default all clean cleandest cleanall spellcheck latex pdf upload $(HELPERS)

default: help ## Make help is the default, explicit is better than implicit

all: pdf cleandest ## Make a pdf of the latest version and clean intermediary latex files from $(DEST) directory

clean: ## Delete $(BUILD) directory
	-rm -rf $(BUILD)

cleandest: ## Delete intermediary latex files from $(DEST) directory
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

cleanall: clean cleandest ## Delete $(BUILD) and $(DEST) directories
	-rm -rf $(DEST)

spellcheck: $(SOURCE) ## Spell checking with aspell
	aspell --mode tex --lang $(SRCLANG) --check $<

latex: $(FINALSOURCE) ## Make the latex source only ($(BUILD)/source-$(FLAVOR).tex)

pdf: $(PDF) $(SOURCES) ## Make the target pdf file

upload: $(PDF) $(BUILD)/latest-$(FLAVOR).html ## Upload the latest version to AWS S3
	-aws s3 cp $(DEST)/$(PDF) s3://files.bagneris.net/finance/$(PATH_TO_LATEST)$(PDF) --cache-control 'public, no-cache'
	-aws s3 cp $(BUILD)/latest-$(FLAVOR).html s3://files.bagneris.net/finance/$(PATH_TO_LATEST)latest.html --cache-control 'public, no-cache'
	# the index should be rebuilt *after* uploading the latest version
	-s3-index > $(BUILD)/index.html
	-aws s3 cp $(BUILD)/index.html s3://files.bagneris.net/index.html --cache-control 'public, no-cache'

$(BUILD) $(DEST):
	mkdir -p $@

$(FINALSOURCE): $(BUILD)/$(SOURCE) $(BUILD)/metadata.yaml $(PANDOCDEPS) $(LATEXDEPS) $(ADDSOURCE)
	pandoc $(PANDOCOPTS) $(LATEXH) $(LATEXB) -A $< $(LATEXA) $(BUILD)/metadata.yaml $(ADDSOURCE) -o $@
	sed -i 's/date{\(.*\)\.1}/date{\1.10}/' $@
	sed -i 's/excludecomment{$(FLAVOR)}/includecomment{$(FLAVOR)}/' $@
	sed -i 's/VERSION/$(VERSION)/' $@
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST);' $@
	sed -i 's;PDF;$(PDF);' $@

$(PDF): $(FINALSOURCE) | $(DEST)
#	We use rubber to build the pdf as its heuristics to guess how many passes
#	are necessary are better than the ones of pandoc it seems.
	$(RUBBER) --jobname $(NAME) $<

$(BUILD)/latest-$(FLAVOR).html: latest-template-$(SRCLANG).html $(PDF)
	-cp -f $(PANDOCREP)/templates/latest-template-$(SRCLANG).html $@
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST);' $@
	sed -i 's;PDF;$(PDF);g' $@

$(BUILD)/$(SOURCE): $(SOURCE) $(SOURCES) | $(BUILD)
	awk 'BEGIN{x="$(BUILD)/$<";}/^---$$/{++i;}i==1{x="$(BUILD)/metadata.yaml";}i==2{print "---">x;i++;x="$(BUILD)/$<";next}{print>x;}' $<

$(BUILD)/metadata.yaml: $(SOURCES)
	-cp -u *yaml $@

include helpers.mk
