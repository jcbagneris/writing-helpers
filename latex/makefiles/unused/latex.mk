# Makefile pour produire des slideshows en LaTeX
# JcB - sam f�v 23 14:05:08 CET 2002
#
#OBJECTS = rdt_requis.pdf
OBJECTS = $(patsubst %.tex,%.pdf,$(wildcard *.tex))

# une r�gle implicite construit les .dvi � partir des .tex en utilisant
# la commande $(TEX)
TEX=latex

# tout faire
all: $(OBJECTS)

# phony targets
.PHONY: clean cleanall cleandvi cleanlog cleanout
# aucune cible interm�diaire ne sera d�truite
.SECONDARY:

# effacer tout sauf le .tex
cleanall: clean 
	-rm *.aux

# effacer les outputs sauf le .aux et le .pdf
clean: cleandvi cleanps cleanlog cleanout

# effacer le .dvi
cleandvi:
	-rm *.dvi

# effacer le .ps
cleanps:
	-rm *.ps

# effacer le .log
cleanlog:
	-rm *.log

# effacer le .out
cleanout:
	-rm *.out

# Comment construire le .ps avec le .dvi
%.ps:%.dvi
	dvips $<

# Comment construire le .pdf avec le .ps
%.pdf:%.ps
	ps2pdf $< $@
