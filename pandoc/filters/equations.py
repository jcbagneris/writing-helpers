"""
Pandoc filter to create numbered equation from Math DisplayMath

"""

import panflute as pf



def prepare(doc):
    pass


def finalize(doc):
    pass

def writelatex(equation, doc):
    math = equation.content[0]
    content = math.text
    if math.format=='DisplayMath' and math.next:
        label = math.next.text.lstrip('{#').strip('}')
        string = '\\begin{equation}\n\label{%s}\n%s\n\end{equation}' % (label, content)
        return pf.RawBlock(string, format='latex')
    else:
        return


writebeamer = writelatex

def nochange(link, doc):
    return

def action(elem, doc):
    WRITERS = {'latex': writelatex, 'beamer': writebeamer}
    if (isinstance(elem, pf.Para)
        and isinstance(elem.content[0], pf.Math)):
        return WRITERS.get(doc.format, 'nochange')(elem, doc)
        


def main(doc=None):
    return pf.run_filter(action,
                        prepare=prepare,
                        finalize=finalize,
                        doc=doc)


if __name__ == '__main__':
    main()

