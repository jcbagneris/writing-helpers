# vim: set noexpandtab
#
# Makefile pour produire des slides en LaTeX avec Beamer
# JcB - dim. mars 31 08:52:05 ICT 2013
#

SLIDES = $(patsubst %-slides.tex,%-slides.pdf,$(wildcard *-slides.tex))
HANDOUT = $(patsubst %-handout.tex,%-handout.pdf,$(wildcard *-handout.tex))
SOURCE = $(filter-out $(wildcard *-slides.tex *-handout.tex), $(wildcard *tex) $(wildcard *png) $(wildcard *jpg))

RUBBER = rubber --pdf

all: slides handout

# for debugging
# utilisation: make print-VARIABLE, par exemple make print-SOURCE
print-%:
	@echo '$*=$($*)'

# phony targets
.PHONY: clean cleanall
# aucune cible intermédiaire ne sera détruite
.SECONDARY:

# effacer tout sauf le .tex
cleanall: clean 
	-rm *.pdf

# effacer les outputs sauf le .pdf
clean:
	-rm *.{aux,log,nav,out,snm,toc,bbl,blg}

# comment construire les slides
slides: $(SLIDES)

# comment construire les handouts
handout: $(HANDOUT)

# comment construire les pdf
%.pdf:%.tex $(SOURCE)
	$(RUBBER) $<
