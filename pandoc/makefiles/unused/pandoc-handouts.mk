# -------------------------------------
# Makefile to produce pdf handouts
# from pandoc markdown sources
#
# JcB - 2017-03-22
#
# -------------------------------------

# -------------------------------------
# source file, should be unique
# -------------------------------------
SOURCE = $(firstword $(wildcard *.pmd))

# -------------------------------------
# paths
# -------------------------------------
PANDOCREP = $(wildcard ~/.pandoc)
LATEXREP =
BIN = $(wildcard ~/bin)
VPATH = $(PANDOCREP)/templates:$(PANDOCREP)/filters:$(PANDOCREP)/includes:$(BUILD):$(DEST)

# -------------------------------------
# vars
# -------------------------------------
# The BUILD directory SHOULD be a subdir of the local src one
BUILD = _build
DEST = _dest
FLAVOR = std

# -------------------------------------
# metadata
# -------------------------------------
AUTHOR != grep ^author: $(SOURCE) | $(BIN)/authors.py
VERSION != grep ^version: $(SOURCE) | cut -d " " -f 2
SRCLANG != grep ^lang: $(SOURCE) | cut -d " " -f 2
DATE != grep "^date: [-/:0-9]*" $(SOURCE) | cut -d " " -f 2
TITLE != grep ^title: $(SOURCE) | cut -d " " -f 2- | tr -s " " "_" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"

# -------------------------------------
# manage FLAVOR
# -------------------------------------
ifeq ($(FLAVOR),std)
	NAME = $(TITLE)--$(AUTHOR)--$(DATE).$(VERSION)
else
	NAME = $(TITLE)--$(AUTHOR)--$(FLAVOR)--$(DATE).$(VERSION)
endif

# -------------------------------------
# intermediary targets
# -------------------------------------
TEXSOURCE = $(BUILD)/source-$(FLAVOR).tex

# -------------------------------------
# dependencies
# -------------------------------------
PANDOCDEPS =
LATEXDEPS = graphicspath.tex example.tex pagestyle.tex title.tex blocks.py tables.tex endfooter-$(SRCLANG).tex

# -------------------------------------
# commands and options
# -------------------------------------
LATEXH = -H $(PANDOCREP)/includes/graphicspath.tex -H $(PANDOCREP)/includes/title.tex -H $(PANDOCREP)/includes/example.tex -H $(PANDOCREP)/includes/tables.tex
LATEXB = -B $(PANDOCREP)/includes/pagestyle.tex
LATEXA = -A $(PANDOCREP)/includes/endfooter-$(SRCLANG).tex
LATEXF = --filter=panflute
ADDSOURCE =
PANDOCOPTS = -f markdown+raw_html+raw_tex --standalone
RUBBER = rubber -m xelatex --into $(DEST)

# -------------------------------------
# output
# -------------------------------------
PDF = $(NAME).pdf
TOPIC != pwd | cut -d/ -f7-8
PATH_TO_LATEST = $(FLAVOR)/$(TOPIC)/$(SRCLANG)/

# -------------------------------------
# targets
# -------------------------------------
.PHONY: default all clean cleandest cleanall spellcheck latex pdf upload $(HELPERS)

default: help ## Make help is the default, explicit is better than implicit

all: pdf cleandest ## Make a pdf of the latest version and clean intermediary latex files from $(DEST) directory

clean: ## Delete $(BUILD) directory
	-rm -rf $(BUILD)

cleandest: ## Delete intermediary latex files from $(DEST) directory
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

cleanall: clean cleandest ## Delete $(BUILD) directory, and pdf target as well as latex compilation files from $(DEST)
	-rm -f $(DEST)/*pdf

spellcheck: $(SOURCE) ## Spell checking with aspell
	aspell --mode tex --lang $(SRCLANG) --check $<

$(BUILD) $(DEST):
	mkdir -p $@

$(TEXSOURCE): $(SOURCE) $(PANDOCDEPS) $(LATEXDEPS) | $(BUILD)
	pandoc $(LATEXH) $(LATEXB) $(LATEXA) $< $(ADDSOURCE) $(PANDOCOPTS) $(LATEXF)  -o $@
	sed -i 's/date{\(.*\)\.1}/date{\1.10}/' $@
	sed -i 's/excludecomment{$(FLAVOR)}/includecomment{$(FLAVOR)}/' $@
	sed -i 's/VERSION/$(VERSION)/' $@
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST)latest.html;' $@

latex: $(TEXSOURCE) ## Make the latex source only ($(BUILD)/source-$(FLAVOR).tex)

pdf: $(PDF) ## Make the target pdf file

$(PDF): $(TEXSOURCE) | $(DEST)
#	We use rubber to build the pdf as its heuristics to guess how many passes
#	are necessary are better than the ones of pandoc it seems.
	$(RUBBER) --jobname $(NAME) $<

$(BUILD)/latest-$(FLAVOR).html: latest-template-$(SRCLANG).html $(PDF) | $(BUILD)
	-cp -f $(PANDOCREP)/templates/latest-template-$(SRCLANG).html $@
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST);' $@
	sed -i 's;PDF;$(PDF);g' $@

upload: $(PDF) $(BUILD)/latest-$(FLAVOR).html ## Upload the latest version to AWS S3
	-aws s3 cp $(DEST)/$(PDF) s3://files.bagneris.net/finance/$(PATH_TO_LATEST)$(PDF) --cache-control 'public, no-cache'
	-aws s3 cp $(BUILD)/latest-$(FLAVOR).html s3://files.bagneris.net/finance/$(PATH_TO_LATEST)latest.html --cache-control 'public, no-cache'
	# the index should be rebuilt *after* uploading the latest version
	-s3-index > $(BUILD)/index.html
	-aws s3 cp $(BUILD)/index.html s3://files.bagneris.net/index.html --cache-control 'public, no-cache'

include helpers.mk
