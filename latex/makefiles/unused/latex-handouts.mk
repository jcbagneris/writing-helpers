# -------------------------------------
# Makefile to produce pdf handouts
# from latex sources
#
# JcB - 2018-08-11
#
# -------------------------------------

# -------------------------------------
# source file, should be unique
# -------------------------------------
SOURCE = $(firstword $(wildcard *.tex))

# -------------------------------------
# paths
# -------------------------------------
PANDOCREP = $(wildcard ~/.pandoc)
BIN = $(wildcard ~/bin)
VPATH = $(PANDOCREP)/templates:$(BUILD):$(DEST)

# -------------------------------------
# vars
# -------------------------------------
BUILD = _build
DEST = _dest
FLAVOR = std

# -------------------------------------
# metadata
# -------------------------------------
AUTHOR != awk 'match($$0, /^\\author{.* (.*)}$$/, arr) {print arr[1]}' $(SOURCE)
VERSION != awk 'match($$0, /{\\versiondate}{v(.*)}$$/, arr) {print arr[1]}' $(SOURCE)
TITLE != awk 'match($$0, /^\\title{(.*)}$$/, arr) {print arr[1]}' $(SOURCE) | tr -s " " "_" | $(BIN)/utf8toascii.py | tr -d ",.;:{}()[]%*"
SRCLANG != pwd | awk -F/ '{print $$NF}'

# -------------------------------------
# manage FLAVOR
# -------------------------------------
ifeq ($(FLAVOR),std)
	NAME = $(TITLE)--$(AUTHOR)--$(VERSION)
else
	NAME = $(TITLE)--$(AUTHOR)--$(FLAVOR)--$(VERSION)
endif

# -------------------------------------
# intermediary targets
# -------------------------------------
TEXSOURCE = $(BUILD)/source-$(FLAVOR).tex

# -------------------------------------
# commands and options
# -------------------------------------
RUBBER = rubber -m xelatex --into $(DEST)

# -------------------------------------
# output
# -------------------------------------
PDF = $(NAME).pdf
URL != pwd | cut -d/ -f7-8
PATH_TO_LATEST = $(FLAVOR)/$(URL)/$(SRCLANG)/

# -------------------------------------
# targets
# -------------------------------------
.PHONY: default all clean cleandest cleanall spellcheck latex pdf upload $(HELPERS)

default: help ## Make help is the default, explicit is better than implicit

all: pdf cleandest ## Make a pdf of the latest version and clean intermediary latex files from $(DEST) directory

clean: ## Delete $(BUILD) directory
	-rm -rf $(BUILD)

cleandest: ## Delete intermediary latex files from $(DEST) directory
	-rm -f $(DEST)/*log $(DEST)/*aux $(DEST)/*toc $(DEST)/*snm $(DEST)/*nav $(DEST)/*out $(DEST)/*bbl $(DEST)/*blg $(DEST)/*cut

cleanall: clean cleandest ## Delete $(BUILD) directory, and pdf target as well as latex compilation files from $(DEST)
	-rm -f $(DEST)/*pdf

spellcheck: $(SOURCE) ## Spell checking with aspell
	aspell --mode tex --lang $(SRCLANG) --check $<

$(BUILD) $(DEST):
	mkdir -p $@

$(TEXSOURCE): $(SOURCE) | $(BUILD)
	cp -f $(SOURCE) $(TEXSOURCE)
	sed -i 's/date{\(.*\)\.1}/date{\1.10}/' $@
	sed -i 's/excludecomment{$(FLAVOR)}/includecomment{$(FLAVOR)}/' $(TEXSOURCE)
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST)latest.html;' $(TEXSOURCE)

latex: $(TEXSOURCE) ## Make the latex source only ($(BUILD)/source.tex)

pdf: $(PDF) ## Make the target pdf file

$(PDF): $(TEXSOURCE) | $(DEST)
#	We use rubber to build the pdf as its heuristics to guess how many passes
#	are necessary are better than the ones of pandoc it seems.
	$(RUBBER) --jobname $(NAME) $(TEXSOURCE)

$(BUILD)/latest-$(FLAVOR).html: latest-template-$(SRCLANG).html $(PDF) | $(BUILD)
	-cp -f $(PANDOCREP)/templates/latest-template-$(SRCLANG).html $@
	sed -i 's;PATH_TO_LATEST;$(PATH_TO_LATEST);' $@
	sed -i 's;PDF;$(PDF);g' $@

$(BUILD)/index.html:
	s3-index > $@

upload: $(PDF) $(BUILD)/latest-$(FLAVOR).html $(BUILD)/index.html ## Upload the latest version to AWS S3
	-aws s3 cp $(DEST)/$(PDF) s3://files.bagneris.net/finance/$(PATH_TO_LATEST)$(PDF) --cache-control 'public, no-cache'
	-aws s3 cp $(BUILD)/latest-$(FLAVOR).html s3://files.bagneris.net/finance/$(PATH_TO_LATEST)latest.html --cache-control 'public, no-cache'
	-aws s3 cp $(BUILD)/index.html s3://files.bagneris.net/index.html --cache-control 'public, no-cache'

include helpers.mk
